// Cadastrar novo registro
function cadastrarRegistro() {
  // Pega os dados do formulário
  let nome = document.querySelector('input[name="nome"]').value;
  let email = document.querySelector('input[name="email"]').value;
  
  // Valida os dados
  if (!nome || !email) {
    alert('Por favor, preencha todos os campos.');
    return;
  }
  
  // Adiciona o registro na tabela
  let id = Date.now();
  let table = document.querySelector('table tbody');
  let row = `
    <tr data-id="${id}">
      <td>${id}</td>
      <td>${nome}</td>
      <td>${email}</td>
      <td>
        <button class="btn-editar">Editar</button>
        <button class="btn-excluir">Excluir</button>
      </td>
    </tr>
  `;
  table.insertAdjacentHTML('beforeend', row);
  
  // Limpa o formulário
  document.querySelector('form').reset();
}

// Atualizar registro existente
function atualizarRegistro() {
  // Pega os dados do formulário
  let id = document.querySelector('input[name="id"]').value;
  let nome = document.querySelector('input[name="nome"]').value;
  let email = document.querySelector('input[name="email"]').value;
  
  // Valida os dados
  if (!id || !nome || !email) {
    alert('Por favor, preencha todos os campos.');
    return;
  }
  
  // Atualiza o registro na tabela
  let row = document.querySelector(`table tbody tr[data-id="${id}
